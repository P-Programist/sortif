import jwt
import uuid
from . import managers
from django.db import models
from datetime import datetime
from datetime import timedelta
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin


class CustomUser(AbstractBaseUser, PermissionsMixin):
    user_id = models.UUIDField(
        verbose_name=_("User ID"),
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
    )

    email = models.EmailField(
        verbose_name=_("Email Adress"),
        max_length=65,
        unique=True,
        db_index=True,
        db_column="admin_email",
    )

    date_joined = models.DateField(verbose_name=_("Date Joined"), default=timezone.now)
    active = models.BooleanField(verbose_name=_("Is_Active"), default=True)
    staff = models.BooleanField(verbose_name=_("Is_Staff"), default=False)
    admin = models.BooleanField(verbose_name=_("Is_Admin"), default=False)

    objects = managers.UserManager()

    USERNAME_FIELD = "email"

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_active(self):
        return self.active

    @property
    def token(self):
        """This property field allows to get user's current token"""
        return self._generate_jwt_token()

    def __str__(self):
        return self.email

    def _generate_jwt_token(self):
        """
        This private method is responsible for generating unique JWT every time this method called.
        """
        dt = datetime.now() + timedelta(days=settings.TOKEN_LIFETIME)
        token = jwt.encode(
            {"id": f"{self.pk}", "exp": dt.utcfromtimestamp(dt.timestamp())},
            settings.SECRET_KEY,
            algorithm="HS256",
        )

        return token.decode("utf-8")


class Client(models.Model):
    def __init__(self, *args, **kwargs):
        super(Client, self).__init__(*args, **kwargs)

    first_name = models.CharField(
        verbose_name=_("Client First Name"),
        max_length=32,
        db_index=True,
    )

    last_name = models.CharField(
        verbose_name=_("Client Last Name"),
        max_length=32,
        db_index=True,
    )

    postal_code = models.CharField(
        verbose_name=_("Postal code"),
        max_length=16,
        blank=True,
    )

    company_name = models.CharField(
        verbose_name=_("Company name"),
        max_length=64,
        blank=True,
    )

    address = models.CharField(
        verbose_name=_("Address"),
        max_length=64,
        blank=True,
    )

    avatar = models.ImageField(
        verbose_name=_("User Profile Photo"),
        upload_to="user_porfiles/",
        default="default.png",
    )

    class Meta:
        verbose_name = _("Client")
        verbose_name_plural = _("Clients")

    def __str__(self):
        return self.username
