from django.contrib.auth import logout
from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from allauth.account.views import PasswordResetView
from authentication.backends import JWTAuthentication
from rest_email_auth.generics import SerializerSaveView
from rest_framework import status, serializers, generics
from .models import CustomUser

from rest_framework.permissions import (
    IsAuthenticated,
    AllowAny,
    IsAuthenticatedOrReadOnly,
    IsAdminUser,
)

from .serializers import (
    LoginSerializer,
    RegistrationSerializer,
    PasswordChangeSerializer,
    CustomUserListSerializer,
)


class CustomUserCreateView(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = RegistrationSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {
                "success": True,
                "token": serializer.data.get("token", None),
            },
            status.HTTP_201_CREATED,
        )


class CustomUserDeletelView(generics.DestroyAPIView):
    permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        if "email" in request.data.keys():
            try:
                user = CustomUser.objects.get_by_natural_key(request.data["email"])
            except CustomUser.DoesNotExist:
                return Response(
                    {"success": True, "message": "User does not exist!", "result": []},
                    status.HTTP_204_NO_CONTENT,
                )
        else:
            user = CustomUser.objects.get_by_natural_key(request.user.email)

        user.delete()
        return Response(
            {
                "success": True,
                "result": f"User {request.data['email']} has been Deleted Successfully!",
            },
            status.HTTP_205_RESET_CONTENT,
        )


class LoginAPIView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer
    authentication_classes = (JWTAuthentication,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(data=serializer.data, status=status.HTTP_200_OK)


class LogoutAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JWTAuthentication,)

    def post(self, request):
        logout(request)
        response = {
            "success": True,
            "result": ("Are you leaving already? Sorry, see you go..."),
        }
        return Response(response, status=status.HTTP_202_ACCEPTED)


class ChangePasswordAPIView(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PasswordChangeSerializer

    def put(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        request.user.set_password(serializer.validated_data["new_password"])
        request.user.save()
        return Response(
            data={"success": True, "result": "Password has been changed Successfully!"},
            status=status.HTTP_204_NO_CONTENT,
        )


class CustomUserDetailView(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = CustomUserListSerializer

    def post(self, request):
        serializer = CustomUserListSerializer(request.user)
        return Response(serializer.data)


class CustomUserListView(generics.ListAPIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = CustomUserListSerializer

    def get(self, request):
        queryset = CustomUser.objects.all().values()
        return Response(
            {"success": True, "result": queryset}, status=status.HTTP_200_OK
        )
