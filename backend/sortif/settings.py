import os
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("SORTIF_SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["*"]

SITE_ID = 1

TOKEN_LIFETIME = 60
AUTH_USER_MODEL = "users.CustomUser"

# Application definition

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.sites",
    "django.contrib.admin",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.contenttypes",
    "users.apps.UsersConfig",
    "billing.apps.BillingConfig",
    "dashboard.apps.DashboardConfig",
    "authentication.apps.AuthenticationConfig",
    "rest_auth",
    "rest_framework",
    "rest_email_auth",
    "rest_auth.registration",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    # "djstripe",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "sortif.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "sortif.wsgi.application"


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.environ.get("SORTIF_DATABASE_1"),
        "USER": os.environ.get("SORTIF_PSQL_USER"),
        "PASSWORD": os.environ.get("SORTIF_PSQL_PASSWORD"),
        "HOST": "localhost",
        "PORT": "5432",
    }
}


AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
        "OPTIONS": {
            "min_length": 8,
        },
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAuthenticated",),
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "authentication.backends.JWTAuthentication",
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.BasicAuthentication",
    ),
}


AUTHENTICATION_BACKENDS = (
    "authentication.backends.JWTAuthentication",
    "django.contrib.auth.backends.ModelBackend",
    "rest_email_auth.authentication.VerifiedEmailBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
)


REST_EMAIL_AUTH = {
    "EMAIL_VERIFICATION_URL": "http://localhost:8000/api/v1/users/verify/",
    "PASSWORD_RESET_URL": "http://localhost:8000/api/v1/users/reset/",
    "REGISTRATION_SERIALIZER": "users.serializers.RegistrationSerializer",
}


REST_AUTH_REGISTER_SERIALIZERS = {
    "LOGIN_SERIALIZER": "users.serializers.LoginSerializer",
    "USER_DETAILS_SERIALIZER": "users.serializers.UserSerializer",
    "REGISTER_SERIALIZER": "users.serializers.RegistrationSerializer",
    "PASSWORD_RESET_SERIALIZER": "accounts.serializers.CustomPasswordResetSerializer",
}


# SOCIALACCOUNT_PROVIDERS = {
#     'google': {
#         # For each OAuth based provider, either add a ``SocialApp``
#         # (``socialaccount`` app) containing the required client
#         # credentials, or list them here:
#         'APP': {
#             'client_id': '123',
#             'secret': '456',
#             'key': ''
#         }
#     }
# }


LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True


STATIC_URL = "/static/"


DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"


MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

LOGIN_REDIRECT_URL = "/api/v1/users/all_users/"
LOGOUT_REDIRECT_URL = "/api/v1/all_users/"
# LOGIN_URL = 'login'

CORS_ORIGIN_ALLOW_ALL = False
# CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = (
    "http://localhost:3000",
    "http://localhost:8000",
)


####################################################################################################
################################### Stripe Billing #################################################
####################################################################################################

# STRIPE_LIVE_SECRET_KEY = os.environ.get("STRIPE_LIVE_SECRET_KEY", "<live secret key>")
# STRIPE_TEST_SECRET_KEY = os.environ.get("STRIPE_TEST_SECRET_KEY", "<test secret key>")
# STRIPE_LIVE_MODE = False  # Change to True in production
# DJSTRIPE_WEBHOOK_SECRET = "whsec_xxx"  # Get it from the section in the Stripe dashboard where you added the webhook endpoint
# DJSTRIPE_USE_NATIVE_JSONFIELD = True  # We recommend setting to True for new installations
# DJSTRIPE_FOREIGN_KEY_TO_FIELD = "id"  # Set to `"id"` for all new 2.4+ installations

####################################################################################################


# Dilovar Don't Forget -> Only During Development, otherwise comment it.
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


# EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_PORT = 587
# EMAIL_HOST_USER = 'dumb.project.team@gmail.com'
# EMAIL_HOST_PASSWORD = '@P@T@_M3G0M'
# EMAIL_USE_TLS = True
# DEFAULT_FROM_EMAIL = 'DUMB Administration <noreply@example.com>'


# ACCOUNT_LOGIN_ATTEMPTS_LIMIT = 3
# ACCOUNT_LOGIN_ATTEMPTS_TIMEOUT = 20
# ACCOUNT_ADAPTER = 'authentication.adapter.CustomAccountAdapter'
# ACCOUNT_USER_MODEL_USERNAME_FIELD = None
# ACCOUNT_EMAIL_REQUIRED = True
# ACCOUNT_UNIQUE_EMAIL = True
# ACCOUNT_USERNAME_REQUIRED = False
# ACCOUNT_AUTHENTICATION_METHOD = 'email'
# ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
# ACCOUNT_CONFIRM_EMAIL_ON_GET = True
# # ACCOUNT_EMAIL_CONFIRMATION_ANONYMOUS_REDIRECT_URL = reverse_lazy('account_confirm_complete')
# # ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL = reverse_lazy('account_confirm_complete')
# ACCOUNT_AUTHENTICATED_LOGIN_REDIRECTS = True
# ACCOUNT_EMAIL_CONFIRMATION_HMAC = True

# PASSWORD_RESET_EXPIRATION = timedelta(hours=1)

# PATH_TO_DUPLICATE_EMAIL_TEMPLATE = 'users/emails/duplicate-email'
# PATH_TO_VERIFY_EMAIL_TEMPLATE = 'users/emails/verify-email'
# PATH_TO_RESET_EMAIL_TEMPLATE = 'users/emails/reset-password'


# EMAIL_VERIFICATION_URL = 'http://localhost:8000/api/v1/users/verify-email/'
# PASSWORD_RESET_URL = 'http://localhost:8000/api/v1/users/reset-password/'

# EMAIL_VERIFICATION_PASSWORD_REQUIRED = True
